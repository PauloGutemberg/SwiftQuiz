//
//  QuizViewController.swift
//  SwiftQuiz
//
//  Created by Paulo Gutemberg on 30/05/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {

	@IBOutlet weak var viewTempo: UIView!
	@IBOutlet weak var lbQuestao: UILabel!
	
	@IBOutlet var btnRespostas: [UIButton]!
	
	var quizManager = QuizManager()
	
	override func viewDidLoad() {
        super.viewDidLoad()

        
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	
		self.viewTempo.frame.size.width = self.view.frame.size.width
		UIView.animate(withDuration: 60.0, delay: 0, options: .curveLinear, animations: {
			//O que quero que aconteca depois dos 60 segundos vai ser definido aqui
			
			//depois de 60 segundos quero que minha view seja 0 no caso a barrinha que esta la em cima do tempo
			self.viewTempo.frame.size.width = 0
			
		}) { (success) in
			//Aqui tem que ser definido o que quero que aconteca depois que a animacao é finalizada
			self.mostrarResultado()
		}
		
		getNovoQuiz()
	}
	
	func getNovoQuiz(){
		quizManager.refreshQuiz()
		self.lbQuestao.text = quizManager.pergunta
		
		for aux in 0..<self.quizManager.opcoes.count {
			let opc = self.quizManager.opcoes[aux]
			let bnt = self.btnRespostas[aux]
			
			bnt.setTitle(opc, for: .normal)
		}
	}
	
	func mostrarResultado(){
		performSegue(withIdentifier: "segueResultado", sender: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if let resultViewController = segue.destination as? ResultViewController {
			
			resultViewController.totalRespostas = quizManager.totalResposta
			resultViewController.totalRespostaCorreta = quizManager.totalRespostaCorreta
		}
	}
	
	@IBAction func selectResposta(_ sender: UIButton) {
		
		let index = self.btnRespostas.index(of: sender)!
		quizManager.validarResposta(indice: index)
		
		getNovoQuiz()
	}
	

}
