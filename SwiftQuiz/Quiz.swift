//
//  Quiz.swift
//  SwiftQuiz
//
//  Created by Paulo Gutemberg on 30/05/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

class Quiz {
	let pergunta: String
	let opcoes: [String]
	private let respostaCorreta: String
	
	init(pergunta:String, opcoes: [String], respostaCorreta: String) {
		self.pergunta = pergunta
		self.opcoes = opcoes
		self.respostaCorreta = respostaCorreta
	}
	
	func validarOpcao(_ indice: Int) -> Bool {
		
		let resposta = self.opcoes[indice]
		return resposta == self.respostaCorreta
	}
	
	deinit {
		print("Liberou o Quiz instanciado na memoria")
	}
	
}
