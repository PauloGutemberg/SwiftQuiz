//
//  ResultViewController.swift
//  SwiftQuiz
//
//  Created by Paulo Gutemberg on 30/05/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

	@IBOutlet weak var lbRespostas: UILabel!
	@IBOutlet weak var lbCorretas: UILabel!
	@IBOutlet weak var lbErradas: UILabel!
	@IBOutlet weak var lbPorcentagem: UILabel!
	
	var totalRespostaCorreta: Int = 0
	var totalRespostas: Int = 0
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		self.lbRespostas.text = "Perguntas respondidas: \(self.totalRespostas)"
		self.lbCorretas.text = "Perguntas corretas: \(self.totalRespostas)"
		self.lbErradas.text = "Perguntas erradas: \(self.totalRespostas - self.totalRespostaCorreta)"
	
		
		let score = self.totalRespostaCorreta * 100 / self.totalRespostas
		
		self.lbPorcentagem.text = "\(score)%"
	}
    


	@IBAction func fechar(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
	}
	
}
